//
//  ViewController.swift
//  ToDo
//
//  Created by Alina Chernenko on 6/29/15.
//  Copyright (c) 2015 dimalina. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var calcButton: UIButton!
    @IBOutlet weak var initialLabel: UILabel!
    var i : Int = 0
    var arrayOfTasks : [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doButtonTap(sender: AnyObject) {
        print("add task")
        
        self.initialLabel.text = "Task \(i)"
        let element = self.initialLabel.text!
        arrayOfTasks.append(element)
        print("ArrayOfTasks [\(i)] = \(arrayOfTasks[i])")
        i++
        
    }
    
    @IBAction func close(segue:UIStoryboardSegue) {
    }
    
    
}
