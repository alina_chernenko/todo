//
//  TextViewController.swift
//  ToDo
//
//  Created by Alina Chernenko on 9/10/15.
//  Copyright (c) 2015 dimalina. All rights reserved.
//

import UIKit

class TextViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!{
        didSet{
        textView.text = text
        }
    }
    
    var text: String = ""
        {
        didSet{
        textView?.text = text
        }
    }
    
    override var preferredContentSize: CGSize {
        get {
            if textView != nil && presentedViewController != nil {
                return textView.sizeThatFits(presentedViewController!.view.bounds.size)
            } else {
            
            return super.preferredContentSize
            }
        }
        set {super.preferredContentSize = newValue}
    
    }
}
