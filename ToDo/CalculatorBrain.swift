//
//  CalculatorBrain.swift
//  ToDo
//
//  Created by Alina Chernenko on 7/2/15.
//  Copyright (c) 2015 dimalina. All rights reserved.
//

import Foundation

class CalculatorBrain
{
    private enum Op: CustomStringConvertible {
        case Operand (Double)
        case UnaryOperation (String, Double -> Double)
        case BinaryOperation (String, (Double, Double) -> Double)
        case ReturnDouble (String)
        case NullaryOperation (String, () -> Double)
        
        var description: String {
            get{
                switch self {
                case .Operand(let operand):
                    return "\(operand)"
                case .UnaryOperation(let symbol,_):
                    return symbol
                case .BinaryOperation(let symbol,_):
                    return symbol
                case .ReturnDouble(let symbol):
                    return  symbol
                case .NullaryOperation(let symbol, _):
                    return symbol
                }
            }
        }

    }
    
    private var opStack = [Op]()
    private var knownOps = [String: Op] ()
    var variableValues = [String:Double] ()
    
    var description: String {
        get {
            var (result, ops) = ("", opStack)
            while ops.count > 0 {
                var current: String?
                (current, ops) = description(ops)
                result = result == "" ? current! : "\(current!), \(result)"
            }
            return result
        }
    }

   
    
    
    init () {
        func learnOp(op: Op){
            knownOps[op.description] = op
        }
        learnOp(Op.BinaryOperation("✕", *))
        learnOp(Op.BinaryOperation("+", +))
        learnOp(Op.BinaryOperation("-") {$1 - $0})
        learnOp(Op.BinaryOperation("÷") {$1 / $0})
        learnOp(Op.UnaryOperation("√", sqrt))
        learnOp(Op.UnaryOperation("+/-") {$0*(-1)})
        learnOp(Op.UnaryOperation("sin ( )") {sin($0)})
        learnOp(Op.UnaryOperation("cos ( )") {cos($0)})
        learnOp(Op.NullaryOperation("π", {M_PI}))
        
    }
    
    typealias PropertyList = AnyObject
    
    var program: PropertyList {// guaranteed to be a PropertyList
        get {

            return opStack.map {$0.description}
        }
        set {
            if let opSymbols = newValue as? Array<String>{
            var newopStack = [Op]()
            for opSymbol in opSymbols {
                if let op = knownOps[opSymbol] {
                    newopStack.append(op)
                } else if let operand = NSNumberFormatter().numberFromString(opSymbol)?.doubleValue {
                    newopStack.append(.Operand(operand))
                }
                
            }
            opStack = newopStack
        }
    }
}
    private func description( ops: [Op]) -> (result: String?, remainingOps: [Op]) {
        if !ops.isEmpty {
            var remainingOps = ops
            let op = remainingOps.removeLast()
            
            switch op {
                
            case .Operand(let operand):
                return (String(format: "%g", operand), remainingOps)
                
            case .UnaryOperation(let symbol, _):
                let operandEvaluation = description(remainingOps)
                if let operand = operandEvaluation.result {
                return ("\(symbol) \(operand)", operandEvaluation.remainingOps )
                }
            case .BinaryOperation(let symbol, _):
                let operand1Evaluation = description(remainingOps)
                if var operand1 = operand1Evaluation.result {
                    if remainingOps.count - operand1Evaluation.remainingOps.count > 2 {
                    operand1 = "(\(operand1))"
                    }
                    let operand2Evaluation = description(operand1Evaluation.remainingOps)
                    if let operand2 = operand2Evaluation.result {
                        return ("\(operand2) \(symbol) \(operand1)", operand2Evaluation.remainingOps)
                    }
                }
            case .ReturnDouble(let symbol):
                return (symbol, remainingOps)
                
            case .NullaryOperation(let symbol, _):
                return (symbol, remainingOps)
            }
        }
        
    return ("?", ops)
    }

    
  private func evaluate ( ops: [Op]) -> (result: Double?, remainingOps: [Op]){
        if !ops.isEmpty {
            var remainingOps = ops
            let op = remainingOps.removeLast()
            switch op {
            case .Operand(let operand):
                return (operand, remainingOps)
                
            case .UnaryOperation(_, let operation):
                let operandEvaluation = evaluate (remainingOps)
                if let operand = operandEvaluation.result
                {
                    return (operation(operand), operandEvaluation.remainingOps)
                }
                
            case .BinaryOperation(_, let operation):
                let op1Evaluation = evaluate(remainingOps)
                if let operand1 = op1Evaluation.result
                {
                    let op2Evaluation = evaluate(op1Evaluation.remainingOps)
                    if let operand2 = op2Evaluation.result {
                        return (operation (operand1, operand2), op2Evaluation.remainingOps)
                    }
                }
                
            case .ReturnDouble(let symbol):
                if let variable = variableValues[symbol] {
                    return (variableValues[symbol], remainingOps)}
  
            case .NullaryOperation(_, let operation):
                return (operation(), remainingOps)
            }
            
            
        }
        return (nil, ops)
    }
    
    
    func evaluate() -> Double? {
        let (result, remainder) = evaluate(opStack)
        print("\(opStack) = \(result) with \(remainder) left over")
        return result
    }
    
    func pushOperand (operand: Double) -> Double? {
    opStack.append(Op.Operand(operand))
        return evaluate()
    }
    
    func pushOperand (symbol: String) -> Double? {
    opStack.append(Op.ReturnDouble(symbol))
    return evaluate()
    }
//    
//    func pushOperand (symbol:String) -> Double?{
//    var variableValues = [String:Double] ()
//    variableValues[symbol] = (symbol as NSString).doubleValue
//    opStack.append(Op.Operand(variableValues[symbol]!))
//        return evaluate()
//    }

    func performOperation(symbol:String) -> Double? {
        if let operation = knownOps[symbol]
        {
            opStack.append(operation)
            
        }
        
        return evaluate()
    }
    
    func removeAll() {
        opStack.removeAll(keepCapacity: false)
    }
}
