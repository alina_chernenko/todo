//
//  DiagnosedHappinessController.swift
//  ToDo
//
//  Created by Alina Chernenko on 9/10/15.
//  Copyright (c) 2015 dimalina. All rights reserved.
//

import UIKit

class DiagnozedHappinessController: HappinessViewController, UIPopoverPresentationControllerDelegate {
    
    private let defaults = NSUserDefaults.standardUserDefaults()
    
    var diagnosticHistory: [Int] {
        get { return defaults.objectForKey(History.DefaultKey) as? [Int] ?? [] }
        set { defaults.setObject(newValue, forKey: History.DefaultKey)}
    }
    
    override var happiness: Int {
        didSet{
            diagnosticHistory += [happiness]
        }
    }
   
    private struct History {
        static let SegueIdentifier = "show diagnostic history"
        static let DefaultKey = "Diagnos.History"
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let identifier = segue.identifier {
            switch identifier {
            case History.SegueIdentifier:
                if let tvc = segue.destinationViewController as? TextViewController {
                    if let ppc = tvc.popoverPresentationController {
                        ppc.delegate = self
                    }
                    tvc.text = "\(diagnosticHistory)"
                    
                }
            default:break
            }
        }
        
        }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
    }