//
//  CalculatorViewController.swift
//  ToDo
//
//  Created by Alina Chernenko on 6/29/15.
//  Copyright (c) 2015 dimalina. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {

    @IBOutlet weak var historyLabel: UILabel!
    @IBOutlet weak var ResultLabel: UILabel!
    var userIsTyping = false
    var afterEnter = false
    var isReset = false
    var isADot = false
    
    var brain = CalculatorBrain()
    
    @IBAction func appendDigit(sender: UIButton) {
        
        let digit = sender.currentTitle!
        
        for indexForDot in (ResultLabel.text!).characters.indices{
            if ResultLabel.text![indexForDot] == "."{
            isADot = true
            break
            }
        }
       
        
        if userIsTyping {
            if !isADot {
                
                ResultLabel.text = ResultLabel.text! + digit
                
            } else if (digit == "."){
                ResultLabel.text = ResultLabel.text!
            }  else {
                ResultLabel.text = ResultLabel.text! + digit
            }
            
            if (digit == "🔙") {
                let numberOfDigits = (ResultLabel.text!).characters.count-1
                if numberOfDigits != 0 {
                    ResultLabel.text = ResultLabel.text!.substringToIndex(ResultLabel.text!.startIndex.advancedBy(numberOfDigits - 1))
                 isADot = false
                } else {
                ResultLabel.text = "0"
                }
            
            }
        }
        else {
            
            
            if historyLabel.text!.rangeOfString("=") != nil{
                historyLabel.text = ResultLabel.text! + " "
            }
            
            if (digit != "🔙") && digit != "." {
                ResultLabel.text = digit
                if isReset {
                    historyLabel.text = ""
                    isReset = false}
                userIsTyping = true
                isADot = false
            }
            
             else if digit == "."{
             ResultLabel.text = ResultLabel.text! + digit
                userIsTyping = true
                isADot = false
            }
        }


        userIsTyping = true
            historyLabel.text = brain.description != "?" ? brain.description : ""
    }
    
    
    
    @IBAction func operate(sender: UIButton) {
        let operation = sender.currentTitle!
        if userIsTyping {
            if operation == "±" {
                let result = ResultLabel.text!
                if (result.rangeOfString("-") != nil) {
                    ResultLabel.text = String(result.characters.dropFirst())
                } else {
                    ResultLabel.text = "-" + result
                }
                return
            }
                Enter()
            
        }
        

        userIsTyping = true
        
        if let result = brain.performOperation(operation){
            ResultValue = result
           
        } else {ResultValue = 0}
        
        
    }
    
    
    @IBAction func reset (){
        brain = CalculatorBrain()
        historyLabel.text = ""
        ResultLabel.text = ""
        isADot = false
        userIsTyping = false
        isReset = true
        ResultValue = nil
        
        
    }
    
    

    @IBAction func SetVariable(sender: UIButton) {
        if let variable = (sender.currentTitle!).characters.last{
            if ResultLabel != nil {
                brain.variableValues["\(variable)"] = ResultValue
                if let result = brain.evaluate() {
                ResultValue = result
                } else { ResultValue = nil}
            }
        }
        userIsTyping = false
        print("\(brain.variableValues)")
    }
    
    
    @IBAction func pushVariable(sender: UIButton) {
        
        if userIsTyping {
        Enter()
        }
        if let result = brain.pushOperand(sender.currentTitle!){
            ResultValue = result
        }
        else {ResultValue = nil}
        print("\(brain.variableValues)")
    }
    
    @IBAction func Enter() {
        
        userIsTyping = false
        if let result = brain.pushOperand(ResultValue!){
            ResultValue = result
        } else {ResultValue = nil}
        
    historyLabel.text = ResultLabel.text! + " " + "="
        
    }
    
    var ResultValue: Double? {
        get {
            return NSNumberFormatter().numberFromString(ResultLabel.text!)?.doubleValue
        }
        
        set {
            if (newValue != nil) {
                let numberFormatter = NSNumberFormatter()
                numberFormatter.numberStyle = .DecimalStyle
                numberFormatter.maximumFractionDigits = 10
                ResultLabel.text = numberFormatter.stringFromNumber(newValue!)
            }
           userIsTyping = false
            historyLabel.text = brain.description != "" ? brain.description + " =" : ""
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
